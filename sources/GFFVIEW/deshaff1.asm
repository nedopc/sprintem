; SHAFF1 block depacker for Z80/Z180 written by Shaos in April-May 2021
; This code is PUBLIC DOMAIN - use it as you want, but on your own RISK!
; See http://nedoPC.org/forum/ for more info about SHAFF packer
; Latest sources: https://gitlab.com/shaos/shaff

; 11-APR-2021 - started woring on SHAFF1 depacker for Z80
; 04-MAY-2021 - initial release published
; 05-MAY-2021 - optimized bit reading subprogram and length calculation

; SHAFF1 depacker listed below is 337 bytes long (non-relocatable).
; Speed of depacking of real life Z80 code is about 465t/byte (22*LDIR).

; This depacker is doing decoding of a single data block with size <=16KB
; No SHAFF header is expected - it's already known that it's SHAFF1 format:
; 0xxxxxxx - single byte #00...#7F
; 10xxxxxxx - single byte #80...#FF
; 110000 - repeat last single byte (no LENGTH after that)
; 110001 LENGTH - repeat last distance longer than -1 and not equal to previous
; 110010 LENGTH - repeat previous last distance longer than -1
; 110011 LENGTH - distance -1 (that basically means copy of the last byte LENGTH times)
; 1101xxxxxx LENGTH - distance from -2 (000000) to -65 (111111)
; 11100xxxxxxxx LENGTH - distance from -66 (00000000) to -321 (11111111)
; 11101xxxxxxxxxx LENGTH - distance from -322 (0000000000) to -1345 (1111111111)
; 1111xxxxxxxxxxxxxx LENGTH - distance up to -16383 (directly encoded with prefix 11)
; special case without LENGTH:
; 111100000000000000 - end of block (after that last byte padded by zero bits)
; and anything above 111111101010111110 is reserved!
; LENGTH is a sequence of 2...26 bits that encode length of the copy:
; 0x - for 2 (encoded by 0) and 3 (encoded by 1)
; 10xx - for 4 (00), 5 (01), 6 (10), 7 (11)
; 110xxx - for 8...15 (000...111)
; 1110xxxx - for 16...31 (0000...1111)
; 11110xxxxx - for 32...63 (00000...11111)
; 111110xxxxxx - for 64...127 (000000...111111)
; 1111110xxxxxxx - for 128...255 (0000000...1111111)
; 11111110xxxxxxxx - for 256...511 (00000000...11111111)
; 111111110xxxxxxxxx - for 512...1023 (000000000...111111111)
; 1111111110xxxxxxxxxx - for 1024...2047 (0000000000...1111111111)
; 11111111110xxxxxxxxxxx - for 2048...4095 (00000000000...11111111111)
; 111111111110xxxxxxxxxxxx - for 4096...8191 (000000000000...111111111111)
; 1111111111110xxxxxxxxxxxxx - for 8192...16383 (0000000000000...1111111111111)

; Input:
;   HL - address of packed data
;   DE - pointer to the buffer where depacked data should appear (up to 16KB)
; Output:
;   HL - address of the next byte after packed data
;   DE - address of the next byte after unpacked data
;   flag C - indicates an error (in this version it's always 0)

DESHAFF1:
  dec hl
  push hl
  pop ix
  ld a,1
  ex af,af' ;'
  ld iy,#FFFF
  ld (prevdist),iy
shaloop:
  push de
  xor a
  call shabit
  jr nc,shabyte0
  call shabit
  rla
  call shabit
  rla
  call shabit
  rla
  ld c,a
  ld b,0
  ld hl,jumps
  add hl,bc
  add hl,bc
  ld e,(hl)
  inc hl
  ld d,(hl)
  ex de,hl
  jp (hl)

jumps dw sha1000,sha1001,sha1010,sha1011,sha1100,sha1101,sha1110,sha1111
prevdist dw 0
lastchar db 0

sha1000:
   ld a,4
   jp shabyte1
sha1001:
   ld a,5
   jp shabyte1
sha1010:
   ld a,6
   jp shabyte1
sha1011:
   ld a,7
   jp shabyte1
shabyte0:
   call shabit
   rla
   call shabit
   rla
shabyte1:
   call shabit
   rla
   call shabit
   rla
   call shabit
   rla
   call shabit
   rla
   call shabit
   rla
   ld (lastchar),a
shabytew:
   pop de
   ld (de),a
   inc de
   jp shaloop

sha1100:
   xor a
   call shabit
   rla
   call shabit
   rla
   or a
   jr nz,sha1100a
   ld a,(lastchar)
   jr shabytew
sha1100a:
   cp 1
   jr nz,sha1100b
   pop de
   push iy
   add iy,de
   push iy
   pop hl
   pop iy
   jr shaldir
sha1100b:
   cp 2
   jr nz,sha1100c
   ld hl,(prevdist)
   jr shalong2
sha1100c:
   pop de
   ld h,d
   ld l,e
   dec hl
shaldir:
   call shalen
   ldir
   jp shaloop

sha1101:
   xor a
   ld b,6
   ld hl,#FFFE
shalong:
   call shabit
   rla
   djnz shalong
   ld d,0
   ld e,a
shalong1:
   xor a
   sbc hl,de
shalong2:
   push iy
   pop de
   ld a,l
   cp e
   jr nz,shasave
   ld a,h
   cp d
   jr z,shalong3
shasave:
   ld (prevdist),iy
   push hl
   pop iy
shalong3:
   pop de
   push de
   add hl,de
   pop de
   call shalen
   ldir
   jp shaloop

sha1110:
   call shabit
   jr nc,sha1110b
   ld b,10
   ld de,0
   ld hl,#FEBE
sha1110l:
   call shabit
   rl e
   rl d
   djnz sha1110l
   jr shalong1
sha1110b:
   ld b,8
   ld hl,#FFBE
   jr shalong

sha1111:
   ld b,14
   ld hl,3
sha1111l:
   call shabit
   rl l
   rl h
   djnz sha1111l
   ld a,h
   cp #C0
   jr nz,shalong2
   ld a,l
   or a
   jr nz,shalong2
   push ix
   pop hl
   inc hl
   pop de
   ret

shalen:
   push hl
   push de
   ld b,0
   ld hl,1
shacnt1:
   inc b
   add hl,hl
   call shabit
   jr c,shacnt1
   ld de,0
shalen1:
   call shabit
   rl e
   rl d
   djnz shalen1
   add hl,de
   ld b,h
   ld c,l
   pop de
   pop hl
   ret

shabit:
   ex af,af' ;'
   dec a
   jr z,shabit0
   ex af,af' ;'
   rlc (ix+0)
   ret
shabit0:
   ld a,8
   inc ix
   ex af,af' ;'
   rlc (ix+0)
   ret
