;    CPMEMUL based on ZSDOS-GP
;
;    Copyright (c) 2002,2021 A.A.Shabarshin <me@shaos.net>
;
;    This file is part of CPMEMUL for Sprinter computer.
;
;    CPMEMUL is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    CPMEMUL is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with CPMEMUL; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	org	8100h-512

;    Standard prefix
	
	dw	5845h
	db	45h
	db	00h
	dw	0200h
	dw	0000h
	dw	0000h
	dw	0000h
	dw	0000h
	dw	0000h
	dw	8100h
	dw	8100h
	dw	0C000h
	ds	490

	include sp.inc

cpmemu:	push	ix
	ld	hl,msg
	ld	c,ESTEX_PCHARS
	rst	10h
	pop	hl
	ld	a,(hl)
	or	a
	jp	z,endof
cpm1:	inc	hl
	ld	a,(hl)
	cp	20h
	jp	z,cpm1
	push	hl
cpm2:	inc	hl
	ld	a,(hl)
	or	a
	jp	z,cpm4
	cp	20h
	jp	nz,cpm2
	ld	(hl),0
cpm4:	ld	hl,startf
	ld	c,ESTEX_PCHARS
	rst	10h
	pop	hl
	push	hl
	ld	c,ESTEX_PCHARS
	rst	10h
	ld	hl,pr
	ld	c,ESTEX_PCHARS
	rst	10h

; Wait to show message until keypress
	ld	c,ESTEX_WAIT
	rst	10h
; Disable accelerator
	ld	bc,#204E
	xor	a ; the same as a=0
	out	(c),a

	ld	b,8
	ld	c,BIOS_EMM_FN2
	rst	8
	ld	hl,err0
	jp	c,endof
	ld	(idmem),a
	ld	b,7
	ld	c,BIOS_EMM_FN4
	rst	8
	ld	(pagebd),a
	out	(PORT_PAGE3),a
	ld	hl,image
	ld	de,bdos
	ld	bc,imagend-image
	ldir
	ld	a,(idmem)
	ld	b,0
	ld	c,BIOS_EMM_FN4
	rst	8
	ld	(pagecc),a
	out	(PORT_PAGE3),a
	ld	hl,code0
	ld	de,0C000h
	ld	bc,256
	ldir
	ld	a,0C9h
	ld	(0C038h),a
	ld	a,(pagebd)
	ld	(0C0F1h),a
	pop	hl
	ld	c,ESTEX_OPEN
	ld	a,1
	rst	10h
	ld	hl,err1
	jp	c,endfri
	ld	(file),a
	ld	hl,0C100h
	ld	de,03F00h
	ld	c,ESTEX_READ
	rst	10h
	ld	hl,err2
	jp	c,endfri
;	or	a
;	jp	z,cpm5
;	ld	hl,err4
;	jp	endfri
cpm5:	ld	a,(file)
	ld	c,ESTEX_CLOSE
	rst	10h
	ld	hl,err3
	jp	c,endfri
	ld	a,(pagebd)
	out	(PORT_PAGE3),a
	ld	a,(pagecc)
	ld	(0C0F1h),a
	ld	a,1
	ld	(0C0F0h),a
	in	a,(PORT_PAGE0)
	ld	(0C0FFh),a
	ld	a,(idmem)
	ld	b,4
	ld	c,BIOS_EMM_FN4
	rst	8
	ld	(0C0FEh),a
	ld	b,5
	ld	c,BIOS_EMM_FN4
	rst	8
	di
	out	(PORT_PAGE0),a

	jp	bios

endfri:	push	hl
	ld	a,(idmem)
	ld	c,BIOS_EMM_FN3
	rst	8
	pop	hl
endof:	ld	c,ESTEX_PCHARS
	rst	10h
	ld	b,1
	ld	c,ESTEX_EXIT
	rst	10h

msg	db	13,10
	db	" CP/M-80 v2.2 Emulator by Shaos <me@shaos.net>",13,10
	db	" Version 0.1 pre-alpha (Jul 3, 2021), http://nedopc.org/nedopc/sprinter/",13,10
	db	" BIOS: Emulation for Sprinter by Alexander Shabarshin",13,10
	db	" BDOS: ZSDOS-GP by Harold F. Bower and Cameron W. Cotrill",13,10
	db	" See GNU General Public License v2 for licensing info",13,10
br	db	13,10,0
pr	db	13,10," Press any key to continue...",13,10,0
startf	db	" Start: ",0
tempstr db	"Shaos",13,10,0
exitstr db	"> Exit",13,10,0
err0	db	"> memory error",0
err1	db	"> file open error",0
err2	db	"> file read error",0
err3	db	"> file close error",0
err4	db	"> too big start file",0
idmem	db	0
pagebd	db	0
pagecc	db	0
file	db	0

code0:	jp	bios+3
code3:	db	0
code4:	db	0
code5:	jp	bdos+6
code8:	push	af
	xor	a
	di
	out	(7Ch),a
	pop	af
	ret
	ret
code_:	ds	0F0h

image:
	incbin	biosemu.bin

imagend	db 0
