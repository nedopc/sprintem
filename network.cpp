/*  network.cpp - Funcrions for Sprinter network emulation (created on May 16, 2021).

    This file is part of SprintEm (emulator of Sprinter computer and clones).

    Copyright (c) 2021, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef linux
#define SOCKETS
#include <unistd.h>
#endif
#ifdef __APPLE__
#define SOCKETS
#include <unistd.h>
#endif
#ifdef SOCKETS
#include <errno.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#endif
#ifdef WIN95
#include <winsock2.h>
#include <ws2tcpip.h>
#define write(a,b,c) send(a,b,c,0)
#define read(a,b,c) recv(a,b,c,0)
#define inet_pton MyInetPton
#endif
#include "sprintem.h"
#include "bios.h"
#include "z80/z80.h"
#include "z80/z_macros.h"

#define CF_OFF   F &= ~FLAG_C
#define CF_ON    F |=  FLAG_C
#define ZF_OFF   F &= ~FLAG_Z
#define ZF_ON    F |=  FLAG_Z

#define SPNET_VER "1.0.0"

char SprinterNet[] = "SprinterNet v" SPNET_VER " build 0 by Shaos (" __DATE__ " " __TIME__ ")";

#define DEBUG

#define INETURLSZ 1024
#define INETHDRSZ 2048
#define INETBUFSZ 32768
#define INETSOCKS 19
#define INETSOCK0 0x10

int InetErr = -1;
char InetURL[INETURLSZ];
char InetHDR[INETHDRSZ];
unsigned char InetBuf[INETBUFSZ];
unsigned char TZ = 0;
unsigned int LastTime = 0;
char LastHumanTime[16] = "";
unsigned char PS[3072];
#ifdef WIN95
SOCKET sockets[INETSOCKS];
#else
int sockets[INETSOCKS];
#ifndef INVALID_SOCKET
#define INVALID_SOCKET (-1)
#endif
#endif

int GetUser(char *buf)
{
   int i;
   for(i=0;i<16;i++)
   {
       buf[i] = PS[3040+i];
       if(!buf[i]) break;
   }
   if(i==16) buf[17]=0;
   return strlen(buf);
}

int SetUser(const char* s)
{
   int i,c=1;
   for(i=0;i<16;i++)
   {
      if(c!=0) c = s[i];
      PS[3040+i] = c;
   }
   return i;
}

int GetVar(const char* s, char* v, int m)
{
   int i,j;
   char str[128],*po;
   unsigned int t1,t2;
#ifdef DEBUG
   printf("\nGetVar '%s' m=%i\n",s,m);
#endif
   if(strchr(s,'=')!=NULL) return 0;
   t1 = (unsigned int)time(NULL);
   for(i=0;i<3040;i+=32)
   {
      if(PS[i]=='$')
      {
            strncpy(str,(char*)&PS[i],sizeof(str));
            str[sizeof(str)-1] = 0;
            po = strchr(str,'=');
            if(po!=0)
            {
               // this is variable
               *po++ = 0;
               if(strcmp(&str[4],s)==0)
               {
                  t2 = (PS[i+1]<<24)|(PS[i+2]<<16)|(PS[i+3]<<8);
                  if(t2 > t1)
                  {
#ifdef DEBUG
                     printf("\tGetVar : found %s (%i seconds to live)\n",&PS[i+4],(int)(t2-t1));
#endif
                     for(j=0;j<m;j++)
                     {
                        v[j] = po[j];
                        if(v[j]==0) break;
                     }
#ifdef DEBUG
                     printf("\tGetVar %s -> %s (%i)\n",s,v,j);
#endif
                     return j;
                  }
                  else
                  {
#ifdef DEBUG
                     printf("\tGetVar : found %s (expired %i seconds ago)\n",&PS[i+4],(int)(t1-t2));
#endif
                     for(j=i;j<3040;j+=32)
                     {
                        // clean all occupied slots starting with current one
                        if(j>i && PS[j]=='$') break; 
                        PS[j+1] = 0;
                     }
#ifdef DEBUG
                     printf("\tGetVar %s -> 0\n",s);
#endif
                     return 0;
                  }
               }
            }
      }
   }
   return 0;
}

int SetVar(const char* s, char* v, int m=0, int t=0)
{
   int i,j,k,ok=0;
   unsigned char b3,b2,b1;
   unsigned int t1;
   char str[128],s2[2],*po;
#ifdef DEBUG
   printf("\nSetVar '%s' to '%s' m=%i t=%i\n",s,v,m,t);
#endif
   if(strchr(s,'=')!=NULL) return 0;
   if(m==0) m = strlen((char*)v); // size of the data
   t1 = (unsigned int)time(NULL);
   if(t==0)
   {
     // infinite life (kind of)
     b3 = 0xFE;
     b2 = b1 = 0xFF;
   }
   else
   {
     t1 += t*60;
     // ignore least significant byte of the timestamp
     t1 &= 0xFFFFFF00;
     t1 += 0x100; // to make sure it's at least 4m16s
     b3 = (t1>>24)&255;
     b2 = (t1>>16)&255;
     b1 = (t1>>8)&255;
   }
   k = 6 + strlen(s) + m;
   k = (k>>5)+((k&31)?1:0);
   for(i=0;i<3040;i+=32)
   {
//printf("%i %c %2.2X %2.2X %2.2X\n",i,PS[i],PS[i+1],PS[i+2],PS[i+3]);
      if(PS[i]=='$')
      {
         if(PS[i+1]==0)
         {
            // slot if free - check if it's enough free space
#ifdef DEBUG
            printf("\tSetVar : check %i slots starting with %i\n",k,i);
#endif
            for(j=1;j<k;j++)
            {
               if(PS[i+(j<<5)]!='$') break;
               if(PS[i+(j<<5)+1]!=0) break;
               if(i+(j<<5)+31 >= 3008) break;
               // TODO: check old entries!!!
            }
            if(j!=k) continue;
            ok = 1; // it is enough room
            break;
         }
         else if(PS[i+1]==0xFF)
         {
            if(i+(k<<5) < 3040)
            {
               ok = 1; // it is enough room at the end
               break;
            }
            else
            {
               // TODO: check old entries!!!
            }
         }
         else
         {
            // slot is occupied
            strncpy(str,(char*)&PS[i],sizeof(str));
            str[sizeof(str)-1] = 0;
            po = strchr(str,'=');
            if(po!=0)
            {
               // this is variable
               *po = 0;
               if(strcmp(&str[4],s)==0)
               {
#ifdef DEBUG
                  printf("\tSetVar : found %s\n",&PS[i+4]);
#endif
                  // TODO: check if new value is longer!!!
                  ok = 1; // write right here
                  break;
               }
            }
         }
      }
   }
   if(!ok) return 0; // fail
   sprintf(str,"%s=",s);
   s2[1] = 0;
   for(j=0;j<m;j++)
   {
      s2[0] = v[j];
      strcat(str,s2);
   }
#ifdef DEBUG
   printf("\tSetVar : write %s to %i\n",str,i);
#endif
   PS[i+1] = b3;
   PS[i+2] = b2;
   PS[i+3] = b1;
   strcpy((char*)&PS[i+4],str);
   return 1;
}

#ifdef WIN95

int MyInetPton(int family, const char* sadr, void* badr)
{
  int i,k;
  unsigned char* p = (unsigned char*)badr;
  char str[16],*po,*poo;
#ifdef DEBUG
  printf("InetPton '%s' -> ",sadr);
#endif
  if(family!=AF_INET || sadr==NULL || badr==NULL) return 0;
  strncpy(str,sadr,16);
  str[15] = 0;
  po = str;
  for(i=0;i<4;i++)
  {
     poo = strchr(po,'.');
     if(i < 3)
     {
       if(poo!=NULL)
            *poo++ = 0;
       else return -1;
     }
     if(strlen(po) > 3) return -2;
     k = atoi(po);
     if(k < 0 || k > 255) return -3;
#ifdef DEBUG
     printf("%i%c",k,(i<3)?'.':' ');
#endif
     p[i] = k;
     po = poo;
  }
#ifdef DEBUG
  printf("\n");
#endif
  return 1;
}

int fWSA = 0;

#endif

int InetDisconnect(int sockfd)
{
  if(sockfd >= 0)
#ifdef WIN95
    closesocket(sockfd);
#else
    close(sockfd);
#endif
  if(sockfd < 0) return -1;
  return 0;
}

int InetConnect(const char *host, unsigned short port)
{
  struct sockaddr_in serv_addr;
  InetErr = 0;
#ifdef WIN95
  SOCKET sockfd;
#else
  int sockfd;
#endif
  if((sockfd = socket(AF_INET, SOCK_STREAM, 0))
#ifdef WIN95
      == INVALID_SOCKET
#else
      < 0
#endif
    ) InetErr++;
  else // SUCCESS
  {
      memset(&serv_addr, 0, sizeof(serv_addr));
      serv_addr.sin_family = AF_INET;
      serv_addr.sin_port = htons(port);
      if(inet_pton(AF_INET, host, &serv_addr.sin_addr)<=0)
      {
         InetDisconnect(sockfd);
         InetErr++;
         sockfd = -2;
      }
      else // SUCCESS
      {
        if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr))
#ifdef WIN95
           == SOCKET_ERROR
#else
           < 0
#endif
          )
        {
          InetDisconnect(sockfd);
          InetErr++;
          sockfd = -3;
        }
      }
  }
  return sockfd;
}

int InetSendChar(int sockfd, char c)
{
  if(sockfd<0) return -1;
  return write(sockfd,&c,1);
}

int InetSendString(int sockfd, const char* s)
{
  if(sockfd<0 || !s || !*s) return -1;
  return write(sockfd,s,strlen(s));
}

int InetSend(int sockfd, void *b, int s)
{
  if(sockfd<0 || !b || s<=0) return -1;
  return write(sockfd,(unsigned char*)b,s);
}

int InetRecv(int sockfd, void *b, int s)
{
  if(sockfd<0) return -1;
  char* rbuf = (char*)b;
  int totaln = 0;
  while(totaln < s)
  {
     int need2read = s - totaln;
     int n = read(sockfd, rbuf, need2read);
#ifdef DEBUG
     if(s!=1) printf("\nReceived %i totaln=%i\n",n,totaln+n);
#endif
     if(n<0)
     {
#ifdef DEBUG
         printf("\nERROR: read failed!\n");
#endif
         return 0;
     }
     if(n==0)
     {
//         continue;
         return totaln;
     }
     totaln += n;
     rbuf += n;
  }
  return totaln;
}

char InetRecvChar(int sockfd)
{
  char b = 0;
  if(InetRecv(sockfd,&b,1) < 1) return 0;
  return b;
}

int InetHTTP(const char *url, unsigned char* buf, int sz)
{
  int i,j,l,b,s,r,ch=0;
  int len = sz;
  int code = 0;
  int port = 80;
  char adr[16];
  char host[32];
  char ibuf[16];
  char sport[8];
  char *po;
#ifdef DEBUG
  printf("\nInetHTTP(%s,%p,%i)\n",url,buf,sz);
#endif
  if(InetErr < 0) return 0;
  if(strncmp(url,"http://",7)!=0 || strlen(url)>INETURLSZ) return -4;
  j = 0;
  l = (int)strlen(url);
  for(i=7;i<l;i++)
  {
     if(url[i]=='/' || url[i]==':' || url[i]=='?' || j>=30) break;
     host[j++] = url[i];
  }
  host[j] = 0;
  if(url[i]==':') port = atoi(&url[i+1]);
  while(url[i]!='/')
  {
     if(url[i++]==0) break;
  }
  struct hostent *he = gethostbyname(host);
  if(he==NULL) return -5;
  struct in_addr **addr_list = (struct in_addr**)he->h_addr_list;
  unsigned char *ip = (unsigned char*)addr_list[0];
#ifdef DEBUG
  printf("%i.%i.%i.%i %i\n",ip[0],ip[1],ip[2],ip[3],port);
  printf("Host: '%s'\n",host);
  printf("Path: '%s'\n",&url[i]);
#endif
  sprintf(adr,"%i.%i.%i.%i",ip[0],ip[1],ip[2],ip[3]);
  s = InetConnect(adr,port);
#ifdef DEBUG
  printf("InetConnect -> %i (err=%i)\n",s,InetErr);
#endif
  if(s<0) return s;
  if(url[i])
  {
     InetSendString(s,"GET ");
     InetSendString(s,&url[i]);
     InetSendString(s," HTTP/1.1\r\n");
  }
  else InetSendString(s,"GET / HTTP/1.1\r\n");
  if(strchr(host,'.')!=NULL)
  {
    InetSendString(s,"Host: ");
    InetSendString(s,host);
    if(port!=80) // it is important to make sure that PHP got correct port number
    {
      sprintf(sport,":%d",port);
      InetSendString(s,sport);
    }
    InetSendString(s,"\r\n");
  }
  InetSendString(s,"User-Agent: SprinterNet/" SPNET_VER " (Emulator SprintEm)\r\n\r\n");
  po = InetHDR;
  while(1)
  {
     b = InetRecvChar(s);
     if(b==0)
     {
#ifdef DEBUG
       printf("InetRecvChar -> %i (err=%i)\n",b,InetErr);
#endif
       break;
     }
     InetHDR[ch++] = b;
     if(b=='\n')
     {
        if(ch>4 && (InetHDR[ch-2]=='\n' || InetHDR[ch-3]=='\n'))
        {
           break;
        }
        if(strncmp(po,"HTTP/",5)==0) // HTTP/1.1 200 OK
        {
           i = 9;
           j = 0;
           while(1)
           {
              b = po[i++];
              if(b==' ' || b=='\r' || b=='\n')
              {
                 ibuf[j++] = 0;
                 break;
              }
              ibuf[j++] = b;
           }
           code = atoi(ibuf);
#ifdef DEBUG
           printf("HTTP code %i\n",code);
#endif
        }
        if(strncmp(po,"Content-Length: ",16)==0) // Content-Length: 22
        {
           i = 16;
           j = 0;
           while(1)
           {
              b = po[i++];
              if(b=='\r' || b=='\n')
              {
                 ibuf[j++] = 0;
                 break;
              }
              ibuf[j++] = b;
           }
           len = atoi(ibuf);
#ifdef DEBUG
           printf("HTTP length %i\n",len);
#endif
        }
        po = &InetHDR[ch];
     }
  }
  InetHDR[ch] = 0;
  while(InetHDR[ch-1]=='\r' || InetHDR[ch-1]=='\n')
  {
     InetHDR[--ch] = 0;
  }
#ifdef DEBUG
   printf("HTTP header {\n%s\n} %i bytes\n",InetHDR,(int)strlen(InetHDR));
#endif
  if(len > sz)
  {
     len = sz;
#ifdef DEBUG
     printf("HTTP length is greater than buffer size %i > %i !!!\n",len,sz);
#endif
  }
  if(code==200)
  {
    r = InetRecv(s,buf,len);
#ifdef DEBUG
    printf("InetRecv -> %i\n",r);
#endif
    if(r > 0 && r < sz) buf[r] = 0;
  }
  else r = -code;
  InetDisconnect(s);
  return r;
}

#ifdef WIN95

void NetworkFini(void)
{
  if(fWSA) WSACleanup();
}

#endif

int NetworkInit(const char* s)
{
   int i;
#ifdef WIN95
   if(!fWSA)
   {
    WSADATA wsa;
    if(WSAStartup(MAKEWORD(2,2),&wsa)!=0)
    {
       InetErr++;
       printf("\nERROR: Can't initialize WSA\n\n");
       return -1;
    }
    fWSA = 1;
    atexit(NetworkFini);
   }
#endif
   for(i=0;i<INETSOCKS;i++)
   {
     sockets[i] = INVALID_SOCKET;
   }
   for(i=0;i<3040;i++)
   { // real SprinterNet will NOT do that because it's "Persistent Storage"
     if((i&31)==0) PS[i]='$';
     else PS[i]=0xFF;
     // every 32 bytes of writable part of the storage represent a record
     // byte 0 is '$' otherwise it's continuation of previous record
     // bytes 1..3 represent 3 higher bytes of unixtime of record expiration with about 5-minute precision
     // some prefixes may indicate special cases:
     // '$',#FF - record terminator (no records after that)
     // '$',#FE - record with infinite life time (Jan 14, 2105)
     // '$',#00 - empty record (deleted or expired)
   }
   PS[3056] = 10;
   PS[3057] = 10;
   PS[3058] = 10;
   PS[3059] = 10;
   PS[3060] = 255;
   PS[3061] = 255;
   PS[3062] = 255;
   PS[3063] = 0;
   PS[3064] = 10;
   PS[3065] = 10;
   PS[3066] = 10;
   PS[3067] = 1;
   PS[3068] = 0;
   PS[3069] = 0;
   PS[3070] = 0;
   PS[3071] = 0; // TZ
   InetErr = 0; // Assume that network is preconfigured already
   return SetUser(s); // fills 3040..3055
}

int Network(int command)
{
#ifdef DEBUG
  printf("\nNetwork(0x%2.2X)\n",command);
#endif
#ifdef WIN95
  SOCKET sockfd;
#else
  int sockfd;
#endif
  struct sockaddr_in sato;
  struct sockaddr ssa;
  int i,j,c,l,sz,ok,r=1;
  unsigned char ubuf[16],*puc;
  char str[256],st[32],st2[32];
  unsigned int ui,t1=(unsigned int)time(NULL);
  CF_ON;
  switch(command)
  {
    case 0x00: // netinit ( HL - buffer ) -> ADE - version of firmware
     InetErr = 0;
     if(HL)
     {
       l = (int)strlen(SprinterNet);
       for(i=0;i<l;i++) writebyte(HL+i,SprinterNet[i]);
       writebyte(HL+i,0);
     }
     A = 1;
     B = 0;
     C = 0;
     CF_OFF;
     break;

    case 0x03: // getconf (32 bytes of configuration is copied to HL and 6 bytes to DE if not zero)
     if(InetErr < 0) break;
     for(i=0;i<32;i++)
     {
        writebyte(HL+i,PS[3040+i]);
     }
     if(DE)
     {
        // fake MAC address
        writebyte(DE,2);
        writebyte(DE+1,0);
        writebyte(DE+2,0);
        writebyte(DE+3,0);
        writebyte(DE+4,0);
        writebyte(DE+5,0);
     }
     CF_OFF;
     A = 0;
     break;

    case 0x06: // getvar (HL-name, DE-buf, A-maxsize)
     if(InetErr < 0) break;

     break;

    case 0x09: // setvar (HL-name, DE-value, A-minutes but 0 means infinite, 255 means 24 hours)
     if(InetErr < 0) break;

     break;

    case 0x0C: // setip (HL-pointer to 12 bytes of configuration) - NOT IMPLEMENTED
     if(InetErr < 0) break;
     A = 255;
     // Change of IP settings is not possible in emulator (it will always use host machine setup)
     CF_ON;
     printf("\nWARNING: Net function 0x0C is not supported in emulator!\n");
     break;

    case 0x0F: // socket (D=domain, E=type) -> A
     // Officially supported domains - AF_LOCAL (1), AF_INET (2) and AF_PACKET (17)
     // Officially supported types - SOCK_STREAM (1), SOCK_DGRAM (2) and SOCK_RAW (3)
     if(InetErr < 0) break;
     // Right now emulation does AF_INET only with SOCK_STREAM or SOCK_DGRAM
     if(D!=AF_INET || (E!=SOCK_STREAM && E!=SOCK_DGRAM))
     {
     // TODO: AF_LOCAL, AF_PACKET and SOCK_RAW
        printf("\nERROR: Unsupported network domain or type (%i,%i)\n",D,E);
        A = 0xFF;
        break;
     }
     sockfd = socket(D,E,0);
     i = -1;
#ifndef WIN95
     if(sockfd < 0)
#else
     if(sockfd == INVALID_SOCKET)
#endif
        A = 0xFF;
     else
     {
        for(i=INETSOCK0;i<INETSOCKS;i++)
        {
           if(sockets[i]==INVALID_SOCKET) break;
        }
        if(i==INETSOCKS)
        {
           InetDisconnect(sockfd);
           A = 0xFF;
        }
        else
        {
           sockets[i] = sockfd;
           CF_OFF;
           A = i;
        }
     }
#ifdef DEBUG
     printf("socket -> %i [%i]\n",sockfd,i);
#endif
     break;

    case 0x12: // bind (A-sock, HL-my_addr, E-addrlen) -> A
     if(InetErr < 0) break;
     if(A >= INETSOCKS) A = 0xFF;
     else
     {
        sockfd = sockets[A];
        if(sockfd==INVALID_SOCKET) A = 0xFF;
        else
        {
           if(A < INETSOCK0) // AF_LOCAL (AF_UNIX)
           {
// TODO: Imitate UNIX socket binding
           }
           else // AF_INET
           {
              if(readbyte(HL)!=AF_INET || readbyte(HL+1)!=0) A = 0xFF;
              else
              {
                sato.sin_family = AF_INET;
                // addr and port should be converted to network order already
                sato.sin_port        = readbyte(HL+2)|
                                      (readbyte(HL+3)<<8);
                sato.sin_addr.s_addr = readbyte(HL+4)|
                                      (readbyte(HL+5)<<8)|
                                      (readbyte(HL+6)<<16)|
                                      (readbyte(HL+7)<<24);
                if(bind(sockfd,(sockaddr*)&sato,sizeof(sato)) < 0) A = 0xFF;
                else
                {
                   CF_OFF;
                   A = 0;
                }
              }
           }
        }
     }
     break;

    case 0x15: // listen
     if(InetErr < 0) break;

     break;

    case 0x18: // accept
     if(InetErr < 0) break;

     break;

    case 0x1B: // shutdown
     if(InetErr < 0) break;
     if(A >= INETSOCKS) A = 0xFF;
     else
     {
        sockfd = sockets[A];
        if(sockfd==INVALID_SOCKET) A = 0xFF;
        else
        {
           if(A < INETSOCK0) // AF_LOCAL (AF_UNIX)
           {
// TODO: Imitate UNIX socket shutdown
           }
           else // AF_INET
           {
#ifndef WIN95
              if(shutdown(sockfd,SHUT_RDWR)==0)
              {
                 CF_OFF;
              }
              else A = 0xFF;
#else
// TODO: shutdown for windows
#endif
           }
        }
     }
     break;

    case 0x1E: // connect (A-sock, HL-theiraddr, E-addrlen) -> A
     if(InetErr < 0) break;
     if(A >= INETSOCKS) A = 0xFF;
     else
     {
        sockfd = sockets[A];
        if(sockfd==INVALID_SOCKET) A = 0xFF;
        else
        {
           for(i=0;i<E;i++)
           {
              InetBuf[i] = readbyte(HL+i);
           }
           if(A < INETSOCK0) // AF_LOCAL (AF_UNIX)
           {
// TODO: Imitate UNIX socket connect ?
           }
           else // AF_INET
           {
              sato.sin_family = AF_INET;
              // addr and port should be converted to network order already
              sato.sin_port        = readbyte(HL+2)|
                                    (readbyte(HL+3)<<8);
              sato.sin_addr.s_addr = readbyte(HL+4)|
                                    (readbyte(HL+5)<<8)|
                                    (readbyte(HL+6)<<16)|
                                    (readbyte(HL+7)<<24);
              if(connect(sockfd, (struct sockaddr *)&sato, sizeof(sato))
#ifdef WIN95
              == SOCKET_ERROR
#else
              < 0
#endif
              )
              A = 0xFF;
              else { A = 0; CF_OFF; }
           }
        }
     }
     break;

    case 0x21: // send (A=sock, HL=buf, DE=size) -> BC
     if(InetErr < 0) break;
     if(A >= INETSOCKS) A = 0xFF;
     else
     {
        sockfd = sockets[A];
        if(sockfd==INVALID_SOCKET) A = 0xFF;
        else
        {
           for(i=0;i<DE;i++)
           {
              InetBuf[i] = readbyte(HL+i);
           }
           if(A < INETSOCK0) // AF_LOCAL (AF_UNIX)
           {
// TODO: Imitate UNIX socket send
           }
           else // AF_INET
           {
              BC = InetSend(sockfd,InetBuf,DE);
           }
           CF_OFF;
        }
     }
     break;

    case 0x24: // recv (A=sock, HL=buf, DE=size) -> BC
     if(InetErr < 0) break;
     if(A >= INETSOCKS) A = 0xFF;
     else
     {
        sockfd = sockets[A];
        if(sockfd==INVALID_SOCKET) A = 0xFF;
        else
        {
           if(A < INETSOCK0) // AF_LOCAL (AF_UNIX)
           {
// TODO: Imitate UNIX socket recv
           }
           else // AF_INET
           {
              BC = InetRecv(sockfd,InetBuf,DE);
              for(i=0;i<BC;i++)
              {
                 writebyte(HL+i,InetBuf[i]);
              }
              CF_OFF;
           }
        }
     }
     break;

    case 0x27: // sendto (A=sock, HL=buf, DE=size, NO flags, IY=out_addr) -> BC
     if(InetErr < 0) break;

     break;

    case 0x2A: // recvfrom (A=sock, HL=buf, DE=size, NO flags, IY=in_addr) -> BC
     if(InetErr < 0) break;
     if(A >= INETSOCKS) A = 0xFF;
     else
     {
        sockfd = sockets[A];
        if(sockfd==INVALID_SOCKET) A = 0xFF;
        else
        {
           if(A < INETSOCK0) // AF_LOCAL (AF_UNIX)
           {
              printf("\nERROR: recvfrom is not supported for AF_UNIX!\n\n");
              A = 0xFE;
           }
           else // AF_INET
           {
#ifndef WIN95
              ui = sizeof(ssa);
              BC = recvfrom(sockfd,InetBuf,DE,0,&ssa,&ui);
#else
              i = sizeof(ssa);
              BC = recvfrom(sockfd,InetBuf,DE,0,&ssa,&i);
#endif
              for(i=0;i<BC;i++)
              {
                 writebyte(HL+i,InetBuf[i]);
              }
              if(BC>0)
              {
                 puc = (unsigned char*)&ssa;
                 for(i=0;i<(int)sizeof(ssa);i++)
                 {
                    writebyte(IY+i,puc[i]);
                 }
                 CF_OFF;
              }
           }
        }
     }
     break;

    case 0x2D: // close (A - sock)
     if(InetErr < 0) break;
     if(A >= INETSOCKS) A = 0xFF;
     else
     {
        sockfd = sockets[A];
        if(sockfd==INVALID_SOCKET) A = 0xFF;
        else
        {
           sockets[A] = INVALID_SOCKET;
           if(A < INETSOCK0) // AF_LOCAL (AF_UNIX)
           {
// TODO: Imitate UNIX socket close
           }
           else // AF_INET
           {
              InetDisconnect(sockfd);
              CF_OFF;
           }
        }
     }
     break;

    case 0x30: // httpget ( IY - pointer to URL string, HL - pointer to buffer, DE - maxsize ) -> BC - data size (or error code)
     if(InetErr < 0) break;
     for(i=0;i<INETURLSZ;i++)
     {
        c = readbyte(IY+i);
        InetURL[i] = c;
        if(c==0) break;
     }
     if(i==INETURLSZ) InetURL[i-1] = 0;
     sz = INETBUFSZ;
     if(DE < sz) sz = DE;
     r = InetHTTP(InetURL,InetBuf,sz);
     if(r > 0)
     {
        CF_OFF;
        BC = r;
        for(i=0;i<r;i++) writebyte(HL+i,InetBuf[i]);
     }
     else
     {
        CF_ON;
        BC = -r;
     }
     break;

    case 0x33: // resolv ( HL - pointer to host string ) -> BCDE - IPv4 address
     if(InetErr < 0) break;
     for(i=0;i<31;i++)
     {
        st[i+1] = readbyte(HL+i);
        if(!st[i+1]) break;
     }
     ubuf[0] = ubuf[1] = ubuf[2] = ubuf[3] = 0;
     st[0] = '?';
     st[31] = 0;
     r = GetVar(st,st2,32);
     if(r!=8)
     {
        GetUser(st2);
        sprintf(str,"http://sprinternet.io:8080/?user=%s&op=resolv&host=%s",st2,&st[1]);
        r = InetHTTP(str,(unsigned char*)st2,32);
        if(r==8) SetVar(st,st2,8,60); // 1 hour (60 minutes)
     }
     if(r==8)
     {
        ui = (unsigned int)hex2i(st2);
        ubuf[0] = (ui>>24)&255;
        ubuf[1] = (ui>>16)&255;
        ubuf[2] = (ui>>8)&255;
        ubuf[3] = ui&255;
        A = 0;
        CF_OFF;
     }
     else
     {
        A = 255;
        CF_ON;
     }
     B = ubuf[0];
     C = ubuf[1];
     D = ubuf[2];
     E = ubuf[3];
     break;

    case 0x36: // time ( A - precision in seconds, HL - optional pointer to buffer for human readable form ) -> BCDE - unixtime
     if(InetErr < 0) break;
     if(t1 < LastTime)
     {
        printf("WARNING: Your computer clock is in the past!!! dt=%u\n",LastTime-t1);
        t1 = LastTime; // fixing computer clock
     }
     ok = 0;
     if(t1 - LastTime < (unsigned int)A) // time was taken recently
     {
        ok = 1;
     }
     else  // last call was done more than A seconds ago
     {
        sprintf(str,"http://sprinternet.io:8080/?op=time");
        r = InetHTTP(str,(unsigned char*)st2,32);
        if(r==22)
        {
           st2[22] = 0;
           strcpy(LastHumanTime,&st2[8]);
           // TODO: correct based on user Time Zone
           st2[8] = 0;
           LastTime = hex2i(st2);
           if(LastTime > t1) ui = LastTime - t1;
           else ui = t1 - LastTime;
           if(ui >= 5)
           {
              printf("WARNING: Your computer clock is misconfigured!!! %s != %8.8X\n",st2,t1);
           }
           ok = 1;
        }
        else
        {
           printf("InetHTTP error (%i)\n",r);
           B = C = D = E = 0;
           CF_ON;
        }
     }
     if(ok)
     {
        CF_OFF;
        // BCDE - unixtime
        B = (LastTime>>24)&255;
        C = (LastTime>>16)&255;
        D = (LastTime>>8)&255;
        E = LastTime&255;
        if(HL) // save human readable format
        {
          // convert to "YYYY-MM-DD hh-mm-ss" - no day of week (yet?)
          j = 0;
          for(i=0;i<=(int)strlen(LastHumanTime);i++) // including zero at the end
          {
             writebyte(HL+j++,LastHumanTime[i]);
             if(i==3 || i==5) writebyte(HL+j++,'-');
             if(i==7) writebyte(HL+j++,' ');
             if(i==9 || i==11) writebyte(HL+j++,':');
          }
//          HL = HL + strlen(str); // DONT move HL
        }
     }
     break;

    case 0x39: // hash ( A - mode, HL - pointer to data, DE - size, IY - pointer to hash ) -> filled from IY
     if(InetErr < 0) break;

     break;

    case 0x3C: // code ( A - mode, HL - pointer to data, DE - size, IY - pointer to processed data ) -> moved IY, filled data
     if(InetErr < 0) break;

     break;

    case 0x3F: // auth ( HL - pointer to password ) -> A-error (if 0 then variable "sid" is added to persitent storage)
     if(InetErr < 0) break;

     break;

    default:
     printf("Unknown Network command 0x%2.2X\n",command);
     r = 0;
     break;
  }
  if(InetErr < 0)
  {
     printf("ERROR: Network is not initialized!\n");
     A = 255;
     CF_ON;
  }
  return r;
}

