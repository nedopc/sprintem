REM Setup paths to internals of Open Watcom v1.9:
SET WATCOM=C:\SYSTEM\WATCOM19
SET PATH=%WATCOM%\BINW;%PATH%
SET INCLUDE=%WATCOM%\H
CD ..\z80
wcl386 /c /ox /ot /fp3 /3r /s z80.cpp
wcl386 /c /ox /ot /fp3 /3r /s z80_ops.cpp
CD ..
wcl386 /c /ox /ot /fp3 /3r /s bios.cpp
wcl386 /c /ox /ot /fp3 /3r /s unigraf.cpp /dFONT8X8
wcl386 /c /ox /ot /fp3 /3r /s targa.cpp
wcl386 /c /ox /ot /fp3 /3r /s sprintem.cpp
wlink system pmodew option stack=64k file z80\z80,z80\z80_ops,bios,unigraf,sprintem name sprintem
