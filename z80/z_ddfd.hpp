/* z80_ddfd.c: z80 DDxx and FDxx opcodes
   Copyright (c) 1999-2001 Philip Kendall
   Extended by Alexander Shabarshin <me@shaos.net> in 2002-2021

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

   Author contact information:

   E-mail: pak@ast.cam.ac.uk
   Postal address: 15 Crescent Road, Wokingham, Berks, RG40 2DB, England

*/

/* define the macros REGISTER, REGISTERL and REGISTERH to be IX,IXL
   and IXH or IY,IYL and IYH to select which register to use
*/

#if !defined(REGISTER) || !defined(REGISTERL) || !defined(REGISTERH)
#error Macros `REGISTER', `REGISTERL' and `REGISTERH' must be defined before including `z80_ddfd.c'.
#endif

case 0x09:		/* ADD REGISTER,BC */
tstates+=15;
ADD16(REGISTER,BC);
#ifdef DON
DPR7("ADD ",REGISTERS,",BC")
#endif
break;

case 0x19:		/* ADD REGISTER,DE */
tstates+=15;
ADD16(REGISTER,DE);
#ifdef DON
DPR7("ADD ",REGISTERS,",DE")
#endif
break;

case 0x21:		/* LD REGISTER,nnnn */
tstates+=14;
REGISTERL=readbyte(PC++);
REGISTERH=readbyte(PC++);
#ifdef DON
DPR8("LD ",REGISTERS,(REGISTERH<<8)|REGISTERL)
#endif
break;

case 0x22:		/* LD (nnnn),REGISTER */
tstates+=20;
#ifdef DON
DPR6("LD (nnnn),",REGISTERS)
#endif
LD16_NNRR(REGISTERL,REGISTERH);
break;

case 0x23:		/* INC REGISTER */
tstates+=10;
REGISTER++;
#ifdef DON
DPR6("INC ",REGISTERS)
#endif
break;

case 0x24:		/* INC REGISTERH */
tstates+=8;
INC(REGISTERH);
#ifdef DON
DPR6("INC ",REGISTERHS)
#endif
break;

case 0x25:		/* DEC REGISTERH */
tstates+=8;
DEC(REGISTERH);
#ifdef DON
DPR6("DEC ",REGISTERHS)
#endif
break;

case 0x26:		/* LD REGISTERH,nn */
tstates+=11;
REGISTERH=readbyte(PC++);
#ifdef DON
DPR8("LD ",REGISTERHS,REGISTERH)
#endif
break;

case 0x29:		/* ADD REGISTER,REGISTER */
tstates+=15;
ADD16(REGISTER,REGISTER);
#ifdef DON
DPR9("ADD ",REGISTERS,",",REGISTERS)
#endif
break;

case 0x2a:		/* LD REGISTER,(nnnn) */
tstates+=20;
#ifdef DON
DPR7("LD ",REGISTERS,",(nnnn)")
#endif
LD16_RRNN(REGISTERL,REGISTERH);
break;

case 0x2b:		/* DEC REGISTER */
tstates+=10;
REGISTER--;
#ifdef DON
DPR6("DEC ",REGISTERS)
#endif
break;

case 0x2c:		/* INC REGISTERL */
tstates+=8;
INC(REGISTERL);
#ifdef DON
DPR6("INC ",REGISTERLS)
#endif
break;

case 0x2d:		/* DEC REGISTERL */
tstates+=8;
DEC(REGISTERL);
#ifdef DON
DPR6("DEC ",REGISTERLS)
#endif
break;

case 0x2e:		/* LD REGISTERL,nn */
tstates+=11;
REGISTERL=readbyte(PC++);
break;

case 0x34:		/* INC (REGISTER+dd) */
tstates+=23;
{
  WORD wordtemp=REGISTER+(SBYTE)readbyte(PC++);
  BYTE bytetemp=readbyte(wordtemp);
  INC(bytetemp);
  writebyte(wordtemp,bytetemp);
#ifdef DON
  DPR7("INC (",REGISTERS,"+dd)")
#endif
}
break;

case 0x35:		/* DEC (REGISTER+dd) */
tstates+=23;
{
  WORD wordtemp=REGISTER+(SBYTE)readbyte(PC++);
  BYTE bytetemp=readbyte(wordtemp);
  DEC(bytetemp);
  writebyte(wordtemp,bytetemp);
#ifdef DON
  DPR7("DEC (",REGISTERS,"+dd)")
#endif
}
break;

case 0x36:		/* LD (REGISTER+dd),nn */
tstates+=19;
{
  WORD wordtemp=REGISTER+(SBYTE)readbyte(PC++);
  writebyte(wordtemp,readbyte(PC++));
#ifdef DON
  DPR7("LD (",REGISTERS,"+dd),nn")
#endif
}
break;

case 0x39:		/* ADD REGISTER,SP */
tstates+=15;
ADD16(REGISTER,SP);
#ifdef DON
DPR7("ADD ",REGISTERS,",SP")
#endif
break;

case 0x44:		/* LD B,REGISTERH */
tstates+=8;
B=REGISTERH;
#ifdef DON
DPR6("LD B,",REGISTERHS)
#endif
break;

case 0x45:		/* LD B,REGISTERL */
tstates+=8;
B=REGISTERL;
#ifdef DON
DPR6("LD B,",REGISTERLS)
#endif
break;

case 0x46:		/* LD B,(REGISTER+dd) */
tstates+=19;
B=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
#ifdef DON
DPR7("LD B,(",REGISTERS,"+dd)")
#endif
break;

case 0x4c:		/* LD C,REGISTERH */
tstates+=8;
C=REGISTERH;
#ifdef DON
DPR6("LD C,",REGISTERHS)
#endif
break;

case 0x4d:		/* LD C,REGISTERL */
tstates+=8;
C=REGISTERL;
#ifdef DON
DPR6("LD C,",REGISTERLS)
#endif
break;

case 0x4e:		/* LD C,(REGISTER+dd) */
tstates+=19;
C=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
#ifdef DON
DPR7("LD C,(",REGISTERS,"+dd)")
#endif
break;

case 0x54:		/* LD D,REGISTERH */
tstates+=8;
D=REGISTERH;
#ifdef DON
DPR6("LD D,",REGISTERHS)
#endif
break;

case 0x55:		/* LD D,REGISTERL */
tstates+=8;
D=REGISTERL;
#ifdef DON
DPR6("LD D,",REGISTERLS)
#endif
break;

case 0x56:		/* LD D,(REGISTER+dd) */
tstates+=19;
D=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
#ifdef DON
DPR7("LD D,(",REGISTERS,"+dd)")
#endif
break;

case 0x5c:		/* LD E,REGISTERH */
tstates+=8;
E=REGISTERH;
#ifdef DON
DPR6("LD E,",REGISTERHS)
#endif
break;

case 0x5d:		/* LD E,REGISTERL */
tstates+=8;
E=REGISTERL;
#ifdef DON
DPR6("LD E,",REGISTERLS)
#endif
break;

case 0x5e:		/* LD E,(REGISTER+dd) */
tstates+=19;
E=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
#ifdef DON
DPR7("LD E,(",REGISTERS,"+dd)")
#endif
break;

case 0x60:		/* LD REGISTERH,B */
tstates+=8;
REGISTERH=B;
#ifdef DON
DPR7("LD ",REGISTERHS,",B")
#endif
break;

case 0x61:		/* LD REGISTERH,C */
tstates+=8;
REGISTERH=C;
#ifdef DON
DPR7("LD ",REGISTERHS,",C")
#endif
break;

case 0x62:		/* LD REGISTERH,D */
tstates+=8;
REGISTERH=D;
#ifdef DON
DPR7("LD ",REGISTERHS,",D")
#endif
break;

case 0x63:		/* LD REGISTERH,E */
tstates+=8;
REGISTERH=E;
#ifdef DON
DPR7("LD ",REGISTERHS,",E")
#endif
break;

case 0x64:		/* LD REGISTERH,REGISTERH */
tstates+=8;
#ifdef DON
DPR7("LD ",REGISTERHS,REGISTERHS)
#endif
break;

case 0x65:		/* LD REGISTERH,REGISTERL */
tstates+=8;
REGISTERH=REGISTERL;
#ifdef DON
DPR9("LD ",REGISTERHS,",",REGISTERLS)
#endif
break;

case 0x66:		/* LD H,(REGISTER+dd) */
tstates+=19;
H=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
#ifdef DON
DPR7("LD H,(",REGISTERS,"+dd)")
#endif
break;

case 0x67:		/* LD REGISTERH,A */
tstates+=8;
REGISTERH=A;
#ifdef DON
DPR7("LD ",REGISTERHS,",A")
#endif
break;

case 0x68:		/* LD REGISTERL,B */
tstates+=8;
REGISTERL=B;
#ifdef DON
DPR7("LD ",REGISTERLS,",B")
#endif
break;

case 0x69:		/* LD REGISTERL,C */
tstates+=8;
REGISTERL=C;
#ifdef DON
DPR7("LD ",REGISTERLS,",C")
#endif
break;

case 0x6a:		/* LD REGISTERL,D */
tstates+=8;
REGISTERL=D;
#ifdef DON
DPR7("LD ",REGISTERLS,",D")
#endif
break;

case 0x6b:		/* LD REGISTERL,E */
tstates+=8;
REGISTERL=E;
#ifdef DON
DPR7("LD ",REGISTERLS,",E")
#endif
break;

case 0x6c:		/* LD REGISTERL,REGISTERH */
tstates+=8;
REGISTERL=REGISTERH;
#ifdef DON
DPR9("LD ",REGISTERLS,",",REGISTERHS)
#endif
break;

case 0x6d:		/* LD REGISTERL,REGISTERL */
tstates+=8;
#ifdef DON
DPR6("LD ","INVALID!!!")
#endif
break;

case 0x6e:		/* LD L,(REGISTER+dd) */
tstates+=19;
L=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
#ifdef DON
DPR7("LD L,(",REGISTERS,"+dd)")
#endif
break;

case 0x6f:		/* LD REGISTERL,A */
tstates+=8;
REGISTERL=A;
#ifdef DON
DPR7("LD ",REGISTERLS,",A")
#endif
break;

case 0x70:		/* LD (REGISTER+dd),B */
tstates+=19;
writebyte( REGISTER + (SBYTE)readbyte(PC++), B);
#ifdef DON
DPR7("LD (",REGISTERS,"+dd),A")
#endif
break;

case 0x71:		/* LD (REGISTER+dd),C */
tstates+=19;
writebyte( REGISTER + (SBYTE)readbyte(PC++), C);
#ifdef DON
DPR7("LD (",REGISTERS,"+dd),C")
#endif
break;

case 0x72:		/* LD (REGISTER+dd),D */
tstates+=19;
writebyte( REGISTER + (SBYTE)readbyte(PC++), D);
#ifdef DON
DPR7("LD (",REGISTERS,"+dd),D")
#endif
break;

case 0x73:		/* LD (REGISTER+dd),E */
tstates+=19;
writebyte( REGISTER + (SBYTE)readbyte(PC++), E);
#ifdef DON
DPR7("LD (",REGISTERS,"+dd),E")
#endif
break;

case 0x74:		/* LD (REGISTER+dd),H */
tstates+=19;
writebyte( REGISTER + (SBYTE)readbyte(PC++), H);
#ifdef DON
DPR7("LD (",REGISTERS,"+dd),H")
#endif
break;

case 0x75:		/* LD (REGISTER+dd),L */
tstates+=19;
writebyte( REGISTER + (SBYTE)readbyte(PC++), L);
#ifdef DON
DPR7("LD (",REGISTERS,"+dd),L")
#endif
break;

case 0x77:		/* LD (REGISTER+dd),A */
tstates+=19;
writebyte( REGISTER + (SBYTE)readbyte(PC++), A);
#ifdef DON
DPR7("LD (",REGISTERS,"+dd),A")
#endif
break;

case 0x7c:		/* LD A,REGISTERH */
tstates+=8;
A=REGISTERH;
#ifdef DON
DPR6("LD A,",REGISTERHS)
#endif
break;

case 0x7d:		/* LD A,REGISTERL */
tstates+=8;
A=REGISTERL;
#ifdef DON
DPR6("LD A,",REGISTERLS)
#endif
break;

case 0x7e:		/* LD A,(REGISTER+dd) */
tstates+=19;
A=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
#ifdef DON
DPR7("LD A,(",REGISTERS,"+dd)")
#endif
break;

case 0x84:		/* ADD A,REGISTERH */
tstates+=8;
ADD(REGISTERH);
#ifdef DON
DPR6("ADD A,",REGISTERHS)
#endif
break;

case 0x85:		/* ADD A,REGISTERL */
tstates+=8;
ADD(REGISTERL);
#ifdef DON
DPR6("ADD A,",REGISTERLS)
#endif
break;

case 0x86:		/* ADD A,(REGISTER+dd) */
tstates+=19;
{
  BYTE bytetemp=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
  ADD(bytetemp);
#ifdef DON
  DPR7("ADD A,(",REGISTERS,"+dd)")
#endif
}
break;

case 0x8c:		/* ADC A,REGISTERH */
tstates+=8;
ADC(REGISTERH);
#ifdef DON
DPR6("ADC A,",REGISTERHS)
#endif
break;

case 0x8d:		/* ADC A,REGISTERL */
tstates+=8;
ADC(REGISTERL);
#ifdef DON
DPR6("ADC A,",REGISTERLS)
#endif
break;

case 0x8e:		/* ADC A,(REGISTER+dd) */
tstates+=19;
{
  BYTE bytetemp=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
  ADC(bytetemp);
#ifdef DON
  DPR7("ADC A,(",REGISTERS,"+dd)")
#endif
}
break;

case 0x94:		/* SUB A,REGISTERH */
tstates+=8;
SUB(REGISTERH);
#ifdef DON
DPR6("SUB A,",REGISTERHS)
#endif
break;

case 0x95:		/* SUB A,REGISTERL */
tstates+=8;
SUB(REGISTERL);
#ifdef DON
DPR6("SUB A,",REGISTERLS)
#endif
break;

case 0x96:		/* SUB A,(REGISTER+dd) */
tstates+=19;
{
  BYTE bytetemp=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
  SUB(bytetemp);
#ifdef DON
  DPR7("SUB A,(",REGISTERS,"+dd)")
#endif
}
break;

case 0x9c:		/* SBC A,REGISTERH */
tstates+=8;
SBC(REGISTERH);
#ifdef DON
DPR6("SBC A,",REGISTERHS)
#endif
break;

case 0x9d:		/* SBC A,REGISTERL */
tstates+=8;
SBC(REGISTERL);
#ifdef DON
DPR6("SBC A,",REGISTERLS)
#endif
break;

case 0x9e:		/* SBC A,(REGISTER+dd) */
tstates+=19;
{
  BYTE bytetemp=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
  SBC(bytetemp);
#ifdef DON
  DPR7("SBC A,(",REGISTERS,"+dd)")
#endif
}
break;

case 0xa4:		/* AND A,REGISTERH */
tstates+=8;
AND(REGISTERH);
#ifdef DON
DPR6("AND A,",REGISTERHS)
#endif
break;

case 0xa5:		/* AND A,REGISTERL */
tstates+=8;
AND(REGISTERL);
#ifdef DON
DPR6("AND A,",REGISTERLS)
#endif
break;

case 0xa6:		/* AND A,(REGISTER+dd) */
tstates+=19;
{
  BYTE bytetemp=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
  AND(bytetemp);
#ifdef DON
  DPR7("AND A,(",REGISTERS,"+dd)")
#endif
}
break;

case 0xac:		/* XOR A,REGISTERH */
tstates+=8;
XOR(REGISTERH);
#ifdef DON
DPR6("XOR A,",REGISTERHS)
#endif
break;

case 0xad:		/* XOR A,REGISTERL */
tstates+=8;
XOR(REGISTERL);
#ifdef DON
DPR6("XOR A,",REGISTERLS)
#endif
break;

case 0xae:		/* XOR A,(REGISTER+dd) */
tstates+=19;
{
  BYTE bytetemp=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
  XOR(bytetemp);
#ifdef DON
  DPR7("XOR A,(",REGISTERS,"+dd)")
#endif
}
break;

case 0xb4:		/* OR A,REGISTERH */
tstates+=8;
OR(REGISTERH);
#ifdef DON
DPR6("OR A,",REGISTERHS)
#endif
break;

case 0xb5:		/* OR A,REGISTERL */
tstates+=8;
OR(REGISTERL);
#ifdef DON
DPR6("OR A,",REGISTERLS)
#endif
break;

case 0xb6:		/* OR A,(REGISTER+dd) */
tstates+=19;
{
  BYTE bytetemp=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
  OR(bytetemp);
#ifdef DON
  DPR7("OR A,(",REGISTERS,"+dd)")
#endif
}
break;

case 0xbc:		/* CP A,REGISTERH */
tstates+=8;
CP(REGISTERH);
#ifdef DON
DPR6("CP A,",REGISTERHS)
#endif
break;

case 0xbd:		/* CP A,REGISTERL */
tstates+=8;
CP(REGISTERL);
#ifdef DON
DPR6("CP A,",REGISTERLS)
#endif
break;

case 0xbe:		/* CP A,(REGISTER+dd) */
tstates+=19;
{
  BYTE bytetemp=readbyte( REGISTER + (SBYTE)readbyte(PC++) );
  CP(bytetemp);
#ifdef DON
  DPR7("CP A,(",REGISTERS,"+dd)")
#endif
}
break;

case 0xcb:		/* {DD,FD}CBxx opcodes */
{
  WORD tempaddr=REGISTER + (SBYTE)readbyte(PC++);
  BYTE opcode3=readbyte(PC++);
#ifdef HAVE_ENOUGH_MEMORY
  switch(opcode3) {
#include "z80_ddfdcb.c"
  }
#else			/* #ifdef HAVE_ENOUGH_MEMORY */
  z80_ddfdcbxx(opcode3,tempaddr);
#endif			/* #ifdef HAVE_ENOUGH_MEMORY */
}
break;

case 0xe1:		/* POP REGISTER */
tstates+=14;
POP16(REGISTERL,REGISTERH);
#ifdef DON
DPR6("POP ",REGISTERS)
#endif
break;

case 0xe3:		/* EX (SP),REGISTER */
tstates+=23;
{
  BYTE bytetempl=readbyte(SP), bytetemph=readbyte(SP+1);
  /* Was writebyte(SP,REGISTER); writebyte(SP+1,REGISTERH); */
  writebyte(SP,REGISTERL); writebyte(SP+1,REGISTERH);
  REGISTERL=bytetempl; REGISTERH=bytetemph;
#ifdef DON
  DPR6("EX (SP),",REGISTERS)
#endif
}
break;

case 0xe5:		/* PUSH REGISTER */
tstates+=15;
PUSH16(REGISTERL,REGISTERH);
#ifdef DON
DPR6("PUSH ",REGISTERS)
#endif
break;

case 0xe9:		/* JP REGISTER */
tstates+=8;
PC=REGISTER;		/* NB: NOT INDIRECT! */
#ifdef DON
DPR6("JP ",REGISTERS)
#endif
break;

/* Note EB (EX DE,HL) does not get modified to use either IX or IY;
   this is because all EX DE,HL does is switch an internal flip-flop
   in the Z80 which says which way round DE and HL are, which can't
   be used with IX or IY. (This is also why EX DE,HL is very quick
   at only 4 T states).
*/

case 0xf9:		/* LD SP,REGISTER */
tstates+=10;
SP=REGISTER;
#ifdef DON
DPR6("LP SP,",REGISTERS)
#endif
break;

default:		/* Instruction did not involve H or L, so backtrack
			   one instruction and parse again */
tstates+=4;
PC--;
break;
