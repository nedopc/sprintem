/*  unigraf.h - Universal Graphics interface (updated on Nov 21, 2021).

    Copyright (c) 2002-2021, Alexander "Shaos" Shabarshin <me@shaos.net>

    This file is part of NedoPC SDK (software development kit for simple devices).

    NedoPC SDK is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    NedoPC SDK is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NedoPC SDK; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __UNIGRAF_H
#define __UNIGRAF_H

#define UG256_WINDOW 0x00
#define UG256_320x200 0x10
#define UG256_640x400 0x20
#define UG256_640x480 0x21

#ifdef __WATCOMC__
#define SCANCODES
#endif
#ifdef SDL
#define SCANCODES
#endif
#ifdef SCANCODES
#define SCANCODE_F1             59
#define SCANCODE_F2             60
#define SCANCODE_F3             61
#define SCANCODE_F4             62
#define SCANCODE_F5             63
#define SCANCODE_F6             64
#define SCANCODE_F7             65
#define SCANCODE_F8             66
#define SCANCODE_F9             67
#define SCANCODE_F10            68
#define SCANCODE_F11            87
#define SCANCODE_F12            88
#define SCANCODE_LEFTSHIFT      42
#define SCANCODE_LEFTCONTROL    29
#define SCANCODE_LEFTALT        56
#define SCANCODE_RIGHTSHIFT     54
#ifndef SDL
#define SCANCODE_RIGHTCONTROL   97
#define SCANCODE_RIGHTALT       100
#else
#define SCANCODE_RIGHTCONTROL   157 // ???
#define SCANCODE_RIGHTALT       184 // ???
#endif
#endif

#define LASTKEY 220

class UniGraf
{
 int dx,dy,dxscr,dyscr,graf;
 int mx,my; // fixed.8
 short px,py,*apy;
 short mox,moy,mol,mom,mor;
 unsigned char *image;
 long *pal;
 int keybuf[3];
 char *keys;
 void* PrivateData;
public:
 UniGraf(int m,int w=0,int h=0);
~UniGraf();
 void Close(void);
 int Screen(int dxs=0,int dys=0);
 int GetScreenWidth(void){return dxscr;}
 int GetScreenHeight(void){return dyscr;}
 int LoadPalette(const char* s);
 int SetPalette(int i,int r,int g,int b);
 long GetPalette(int i);
 int SetScreenPixel(int x,int y,int c);
 int GetScreenPixel(int x,int y);
 int DrawChar8x8(int x,int y,int c,int s,int b=-1);
 int DrawChar8x16(int x,int y,int c,int s,int b=-1);
 int DrawString(int x,int y,const char *c,int h,int s,int b=-1);
 int Update(void);
 int KeyPressed(int scan);
 int WhatKey(void);
 int KeyClear(void);
 int WaitKey(void);
 int GetMouseX(void){return mox;}
 int GetMouseY(void){return moy;}
 int GetMouseL(void){return mol;}
 int GetMouseM(void){return mom;}
 int GetMouseR(void){return mor;}
};

#endif
