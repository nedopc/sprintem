#include <stdio.h>
#include <time.h>
int main(int argc,char **argv)
{
 int i,j,k,c,ok;
 int number_of_packets = 100;
 int bits_per_packet = 10;
 int total_bits = 0;
 int error_bits = 0;
 int error_packets = 0;
 srand(time(NULL));
 if(argc>1) number_of_packets = atoi(argv[1]);
 if(argc>2) bits_per_packet = atoi(argv[2]);
 printf("\n");
 for(j=0;j<number_of_packets;j++)
 {
   ok = 1;
   for(i=0;i<bits_per_packet;i++)
   {
      k = rand()%100;
      if(k==99) c='*'; /* one percent of errors */
      else if(k>=50) c='1';
      else c='0';
      printf("%c",c);
      if(c=='*') { ok=0; error_bits++; }
      total_bits++;
   }
   if(ok) printf(" - GOOD\n");
   else { printf(" - BAD\n"); error_packets++; }
 }
 printf("\n");
 printf("Bits per packet - %i\n",bits_per_packet);
 printf("Total bits - %i\n",total_bits);
 printf("Error bits - %i (%2.2f%%)\n",error_bits,(error_bits*100.0/total_bits));
 printf("Total packets - %i\n",number_of_packets);
 printf("Failed packets - %i (%2.2f%%)\n",error_packets,(error_packets*100.0/number_of_packets));
 printf("\n");
 return 0;
}
