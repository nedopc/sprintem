/* Convert extended graphics for Sprinter to GFX file format (then shaff -1 it to get GFF).
   Supported horizontal resolutions: 320, 336, 352, 368.
   Supported vertical resolutions: 256, 264, 272, 280, 288.
   Created by Shaos in April 2021, latest code could be found here:
   https://gitlab.com/nedopc/zpring/-/tree/master/tools
   This code is PUBLIC DOMAIN - use it on your own RISK! */

/* BEGIN USER AREA */
#include "tv_chart.xpm"
char** xpm = tv_chart_xpm;
#define FNAME "TV_CHART.GFX"
/* END USER AREA */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* EGA colors for Text mode 0:    1:    2:    3:    4:    5:    6:    7:    8:    9:   10:   11:   12:   13:   14:   15: */
unsigned char egar[16] = { 0x00, 0x00, 0x00, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55, 0xFF, 0xFF, 0xFF, 0xFF };
unsigned char egag[16] = { 0x00, 0x00, 0xAA, 0xAA, 0x00, 0x00, 0x55, 0xAA, 0x55, 0x55, 0xFF, 0xFF, 0x55, 0x55, 0xFF, 0xFF };
unsigned char egab[16] = { 0x00, 0xAA, 0x00, 0xAA, 0x00, 0xAA, 0x00, 0xAA, 0x55, 0xFF, 0x55, 0xFF, 0x55, 0xFF, 0x55, 0xFF };

unsigned char *gfx = NULL;

short pal[256];

/* Convert hexadecimal digit to integer */
int hexx(char c)
{
  int i = -1;
  switch(c)
  {
    case '0': i=0;break;
    case '1': i=1;break;
    case '2': i=2;break;
    case '3': i=3;break;
    case '4': i=4;break;
    case '5': i=5;break;
    case '6': i=6;break;
    case '7': i=7;break;
    case '8': i=8;break;
    case '9': i=9;break;
    case 'a': i=10;break;
    case 'b': i=11;break;
    case 'c': i=12;break;
    case 'd': i=13;break;
    case 'e': i=14;break;
    case 'f': i=15;break;
    case 'A': i=10;break;
    case 'B': i=11;break;
    case 'C': i=12;break;
    case 'D': i=13;break;
    case 'E': i=14;break;
    case 'F': i=15;break;
  }
  return i;
}

#define PALOFFSET 996 /* 996 for palette 1, 1000 for palette 2, 1004 for palette 3 */
#define GFXMODE 0x60 /* graphics mode 320 pixels with pallete 1 */
#define PAGE2 /* enable copy of the same references in 2nd page */

int main()
{
 FILE *f;
 char *po,str[100];
 int i,j,k,r,g,b,dx,dy,col,cnum,offs,vo1,vo2,ii,jj,kk;
 printf("Generating %s\n",FNAME);
 sscanf(xpm[0],"%d %d %d %d",&dx,&dy,&col,&cnum);
 printf("%ix%i (%i colors - %i characters each)\n",dx,dy,col,cnum);
 if(dx < 320 || dy < 256)
 {
    printf("Size is too small!!\n");
    return -10;
 }
 if(dx > 368 || dy > 288)
 {
    printf("Size is too big!!!\n");
    return -10;
 }
 offs = (dx-320)>>1;
 if(offs&7)
 {
    printf("Horizontal dimension is not multiple of 16!\n");
    return -11;
 }
 if(dy&3)
 {
    printf("Vertical dimension is not multiple of 8!\n");
    return -12;
 }
 if(dy==288) vo1=vo2=16;
 else if(dy==280){vo1=16;vo2=8;}
 else if(dy==272) vo1=vo2=8;
 else if(dy==264){vo1=8;vo2=0;}
 else vo1=vo2=0; /* dy=256 */
 gfx = (unsigned char*)malloc(256*1024);
 if(gfx==NULL) return -1;
 memset(gfx,0,256*1024);
 for(i=0;i<256;i++)
 {
    pal[i]=0;
    for(j=0;j<160;j+=4)
    {
      if(j==0x80 || j>=0x98 || i==0 || i==81 || (i>=110 && i<=128) || i==209 || i>=238) gfx[(i<<10)+768+j] = 0xF8;
      if((j>=0x84 && j<=0x94) || (i>=82 && i<=109) || (i>=210 && i<=237))
      {
         if(j>=0x98)
              gfx[(i<<10)+768+j] = 0xFE;
         else gfx[(i<<10)+768+j] = 0xFC;
      }
      if(i>=1 && i<=80 && j<0x80)
      {
         k = (i-1)/2;
         gfx[(i<<10)+768+j] = (k>>3)|GFXMODE;
         gfx[(i<<10)+769+j] = (k&7)|((j>>2)<<3);
      }
      if(i>=129 && i<=208 && j<0x80)
      {
         k = (i-129)/2+40;
         gfx[(i<<10)+768+j] = (k>>3)|GFXMODE;
         gfx[(i<<10)+769+j] = (k&7)|((j>>2)<<3);
      }
    }
 }

#define GFXDEF1 \
      gfx[(i<<10)+768+(jj<<2)] = (k>>3)|GFXMODE; \
      gfx[(i<<10)+769+(jj<<2)] = (k&7)|(j<<3); \
      gfx[((i+1)<<10)+768+(jj<<2)] = (k>>3)|GFXMODE; \
      gfx[((i+1)<<10)+769+(jj<<2)] = (k&7)|(j<<3);
#define GFXDEF2 \
      gfx[((i+128)<<10)+768+(jj<<2)] = (k>>3)|GFXMODE; \
      gfx[((i+128)<<10)+769+(jj<<2)] = (k&7)|(j<<3); \
      gfx[((i+129)<<10)+768+(jj<<2)] = (k>>3)|GFXMODE; \
      gfx[((i+129)<<10)+769+(jj<<2)] = (k&7)|(j<<3);

 if(offs>=8)
 {
   /* square added on left and right - 111,0 and 81,82 */
   for(j=0;j<128;j+=4)
   {
      k = 42;
      gfx[(111<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(111<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(0<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(0<<10)+769+j] = (k&7)|((j>>2)<<3);
#ifdef PAGE2
      gfx[(239<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(239<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(129<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(129<<10)+769+j] = (k&7)|((j>>2)<<3);
#endif
      k = 77;
      gfx[(81<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(81<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(82<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(82<<10)+769+j] = (k&7)|((j>>2)<<3);
#ifdef PAGE2
      gfx[(209<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(209<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(210<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(210<<10)+769+j] = (k&7)|((j>>2)<<3);
#endif
   }
   if(vo1>=8)
   {
      jj=39;j=3;k=45;
      gfx[(111<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(111<<10)+769+(jj<<2)] = (k&7)|(j<<3);
      gfx[(0<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(0<<10)+769+(jj<<2)] = (k&7)|(j<<3);
#ifdef PAGE2
      gfx[(239<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(239<<10)+769+(jj<<2)] = (k&7)|(j<<3);
      gfx[(129<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(129<<10)+769+(jj<<2)] = (k&7)|(j<<3);
#endif
      k=74;i=81;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      jj=32;j=28;k=45;
      gfx[(111<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(111<<10)+769+(jj<<2)] = (k&7)|(j<<3);
      gfx[(0<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(0<<10)+769+(jj<<2)] = (k&7)|(j<<3);
#ifdef PAGE2
      gfx[(239<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(239<<10)+769+(jj<<2)] = (k&7)|(j<<3);
      gfx[(129<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(129<<10)+769+(jj<<2)] = (k&7)|(j<<3);
#endif
      k=74;i=81;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   if(vo1>=16)
   {
      jj=38;j=2;k=45;
      gfx[(111<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(111<<10)+769+(jj<<2)] = (k&7)|(j<<3);
      gfx[(0<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(0<<10)+769+(jj<<2)] = (k&7)|(j<<3);
#ifdef PAGE2
      gfx[(239<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(239<<10)+769+(jj<<2)] = (k&7)|(j<<3);
      gfx[(129<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(129<<10)+769+(jj<<2)] = (k&7)|(j<<3);
#endif
      k=74;i=81;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      jj=33;j=29;k=45;
      gfx[(111<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(111<<10)+769+(jj<<2)] = (k&7)|(j<<3);
      gfx[(0<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(0<<10)+769+(jj<<2)] = (k&7)|(j<<3);
#ifdef PAGE2
      gfx[(239<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(239<<10)+769+(jj<<2)] = (k&7)|(j<<3);
      gfx[(129<<10)+768+(jj<<2)] = (k>>3)|GFXMODE;
      gfx[(129<<10)+769+(jj<<2)] = (k&7)|(j<<3);
#endif
      k=74;i=81;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
 }
 if(offs>=16)
 {
   /* another square added on left and right - 109,110 and 83,84 */
   for(j=0;j<128;j+=4)
   {
      k = 41;
      gfx[(109<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(109<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(110<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(110<<10)+769+j] = (k&7)|((j>>2)<<3);
#ifdef PAGE2
      gfx[(237<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(237<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(238<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(238<<10)+769+j] = (k&7)|((j>>2)<<3);
#endif
      k = 78;
      gfx[(83<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(83<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(84<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(84<<10)+769+j] = (k&7)|((j>>2)<<3);
#ifdef PAGE2
      gfx[(211<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(211<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(212<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(212<<10)+769+j] = (k&7)|((j>>2)<<3);
#endif
   }
   if(vo1>=8)
   {
      jj=39;j=3;k=44;i=109;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      k=75;i=83;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      jj=32;j=28;k=44;i=109;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      k=75;i=83;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   if(vo1>=16)
   {
      jj=38;j=2;k=44;i=109;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      k=75;i=83;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      jj=33;j=29;k=44;i=109;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      k=75;i=83;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
 }
 if(offs>=24)
 {
   /* another square added on left and right - 107,108 and 85,86 */
   for(j=0;j<128;j+=4)
   {
      k = 40;
      gfx[(107<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(107<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(108<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(108<<10)+769+j] = (k&7)|((j>>2)<<3);
#ifdef PAGE2
      gfx[(235<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(235<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(236<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(236<<10)+769+j] = (k&7)|((j>>2)<<3);
#endif
      k = 79;
      gfx[(85<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(85<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(86<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(86<<10)+769+j] = (k&7)|((j>>2)<<3);
#ifdef PAGE2
      gfx[(213<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(213<<10)+769+j] = (k&7)|((j>>2)<<3);
      gfx[(214<<10)+768+j] = (k>>3)|GFXMODE;
      gfx[(214<<10)+769+j] = (k&7)|((j>>2)<<3);
#endif
   }
   if(vo1>=8)
   {
      jj=39;j=3;k=43;i=107;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      k=76;i=85;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      jj=32;j=28;k=43;i=107;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      k=76;i=85;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   if(vo1>=16)
   {
      jj=38;j=2;k=43;i=107;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      k=76;i=85;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      jj=33;j=29;k=43;i=107;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
      k=76;i=85;
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
 }

 if(vo1>=8)
 {
   /* adding square on the top */
   jj = 39;
   j = 3;
   for(k=46,i=1;k<=48;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   j = 1;
   for(k=43,i=7;k<=76;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   j = 3;
   for(k=71,i=75;k<=73;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
 }
 if(vo1>=16)
 {
   /* another square on the top */
   jj = 38;
   j = 2;
   for(k=46,i=1;k<=48;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   j = 0;
   for(k=43,i=7;k<=76;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   j = 2;
   for(k=71,i=75;k<=73;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
 }
 if(vo2>=8)
 {
   /* adding square under the bottom */
   jj = 32;
   j = 28;
   for(k=46,i=1;k<=48;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   j = 30;
   for(k=43,i=7;k<=76;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   j = 28;
   for(k=71,i=75;k<=73;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
 }
 if(vo2>=16)
 {
   /* another square under the bottom */
   jj = 33;
   j = 29;
   for(k=46,i=1;k<=48;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   j = 31;
   for(k=43,i=7;k<=76;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
   j = 29;
   for(k=71,i=75;k<=73;k++,i+=2)
   {
      GFXDEF1
#ifdef PAGE2
      GFXDEF2
#endif
   }
 }
 /* shifted INT */
 gfx[ (86<<10)+768+0x80] = 0xFD;
 gfx[ (87<<10)+768+0x80] = 0xFD;
 gfx[(214<<10)+768+0x80] = 0xFD;
 gfx[(215<<10)+768+0x80] = 0xFD;
 for(i=1;i<=col;i++)
 {
    k = (xpm[i][1]-0x20)<<8;
    k |= xpm[i][0]-0x20;
    po = strrchr(xpm[i],'#');
    if(po==NULL){free(gfx);return -2;}
    strcpy(str,&po[1]);
    r = (hexx(str[0])<<4) + hexx(str[1]);
    g = (hexx(str[2])<<4) + hexx(str[3]);
    b = (hexx(str[4])<<4) + hexx(str[5]);
    j = i - 1;
    gfx[(j<<10)+PALOFFSET+0] = r;
    gfx[(j<<10)+PALOFFSET+1] = g;
    gfx[(j<<10)+PALOFFSET+2] = b;
    pal[j] = k;
    /* text palette */
    if(j<=15)
    {
       gfx[(j<<10)+1012] = egar[j];
       gfx[(j<<10)+1013] = egag[j];
       gfx[(j<<10)+1014] = egab[j];
       gfx[(j<<10)+1020] = egar[j];
       gfx[(j<<10)+1021] = egag[j];
       gfx[(j<<10)+1022] = egab[j];
    }
 }
 for(j=0;j<dy;j++)
 {
    k = 0;
    jj = j - vo1;
    for(i=0;i<dx;i++)
    {
       kk = xpm[j+col+1][k++]-0x20;
       kk |= (xpm[j+col+1][k++]-0x20)<<8;
       for(ii=0;ii<256;ii++) if(pal[ii]==kk) break;
       if(ii==256){free(gfx);return -3;}
       if(jj < 0)
       {
         if(i<24+offs)
         {
            if(vo1==8)
               gfx[((j+24)<<10)+i+368-offs] = ii;
            else /* vo1==16 */
               gfx[((j+16)<<10)+i+368-offs] = ii;
         }
         else if(i<296+offs)
         {
            if(vo1==8)
               gfx[((j+8)<<10)+i+320-offs] = ii;
            else /* vo1==16 */
               gfx[(j<<10)+i+320-offs] = ii;
         }
         else
         {
            if(vo1==8)
               gfx[((j+24)<<10)+i+272-offs] = ii;
            else /* vo1==16 */
               gfx[((j+16)<<10)+i+272-offs] = ii;
         }
       }
       else if(j < dy-vo2)
       {
         if(i<offs) gfx[(jj<<10)+i+344-offs] = ii;
         else if(i<320+offs) gfx[(jj<<10)+i-offs] = ii;
         else gfx[(jj<<10)+i+296-offs] = ii;
       }
       else /* j > dy-vo2 */
       {
         if(i<24+offs) gfx[((j-vo1-32)<<10)+i+368-offs] = ii;
         else if(i<296+offs) gfx[((j-vo1-16)<<10)+i+320-offs] = ii;
         else gfx[((j-vo1-32)<<10)+i+272-offs] = ii;
       }
    }
 }
 f = fopen(FNAME,"wb");
 if(f==NULL){free(gfx);return -4;}
 fwrite(gfx,256,1024,f);
 fclose(f);
 free(gfx);
 printf("Ok\n");
 return 0;
}
