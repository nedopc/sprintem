SprintEm
========

SprintEm (formerly known as SPRINT or later Zpring) is an open source
emulator of an old "super-spectrum" computer Sprinter from 2001-2002
(it includes all modern re-incarnation of that desing as Sp2016, Sp2022 etc)
and potentially some new Sprinter-compatible prototypes that may be created.
Alexander "Shaos" Shabarshin started development of SPRINT on February 18, 2002
around Z80 emulation core from GPL emulator FUSE by Philip Kendall.
That's why GNU GPL v2 was also chosen to cover entire emulator SPRINT (now SprintEm).

SprintEm may be compiled by ways listed below:

- Makefile for GCC under Linux and SDL (X11 + libsdl1.2-dev + libpng-dev);
- The same Makefile for GCC under MacOS (required brew for sdl and libpng);
- build/mkwin.bat for Borland C++ v5.5 under 32-bit Windows + SDL v1.2 (NO PNG YET);
- build/mkdos.bat for Watcom-C/C++ under the DOS (NO NETWORK, NO PNG YET).

Don't forget to edit bat-files to set proper paths for your system if needed!

Ways to launch SprintEm:

- with parameter FILE.EXE (where FILE.EXE is a name of any Sprinter EXE-file);
- without parameter (name of EXE will be taken from autoexec parameter of sprintem.ini);
- in Linux you can click SPRINT-FN.sh in file manager to execute Flex Navigator;
- in Windows you can drag-n-drop EXE-file to sprintem.exe and it will be executed.

32-bit Windows binary sprintem.exe should work everywhere from Windows 95 to Windows 10
(with current sprintem.ini it will execute Flex Navigator if launched without parameters).

ZIP-archive with working Windows binary could be downloaded from here: https://gitlab.com/nedopc/sprintem/-/archive/master/sprintem-master.zip

Keyboard shortcuts:

- Shift-F2 - Save 64K memory to memory.bin file;
- Shift-F9 - Save screenshot in PNG format (Sp0001.png, Sp0002.png and so on);
- Shift-F10 - Quit (but in Flex Navigator it could be just F10).

During emulation SprintEm is able to create debug.out file if sprintem.ini
is having debug=1 in it - that is disassembled trace of Z80 processor and
it could be huge!

NOTES:

- Currenly PNG screenshots (Shift-F9) work only in Linux and MacOS;
- Network API is available in Linux, MacOS and Win32.

TODO:

1) Finilize Network API
2) Support for 2 videoscreens
3) Extended video modes (up to 736x288)

Author: Alexander "Shaos" Shabarshin

E-mail: me@shaos.net

http://nedopc.org/forum/

http://sprinter.nedopc.org
